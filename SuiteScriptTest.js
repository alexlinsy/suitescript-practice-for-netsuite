/**
 * @NApiVerison 2.0
 * @NScriptType ClientScript 
 */

/**
 * Hello World Entry Point
 * Entry Point: pageInit
 * Module: N/ui/dialog
 */
define(['N/ui/dialog'], (dialog) => {
    const helloWorld = () => {
        let options = {
            title: 'Hello',
            message: 'Hello, World!'
        };
        try {
            dialog.alert(options);
            log.debug({
                title: 'Success',
                details: 'Alert displayed successfully'
            });
        } catch (e) {
            log.error({
                title: e.name,
                details: e.message
            });
        }
    }
    return {
        pageInit: helloWorld
    };
});